package com.shenfeld.weatherio.network

import com.shenfeld.weatherio.data.DailyDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApiInterface {

    @GET("data/2.5/onecall")
    fun requestDailyWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") appId: String,
        @Query("exclude") exclude: String,
        @Query("units") units: String
    ): Call<DailyDto>
}