package com.shenfeld.weatherio.network

import com.shenfeld.weatherio.network.logger.CustomHttpLogging
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object WeatherApiClient {
    private const val BASE_URL = "http://api.openweathermap.org/"
    private val interceptor: HttpLoggingInterceptor =
        HttpLoggingInterceptor(CustomHttpLogging()).setLevel(HttpLoggingInterceptor.Level.BODY)
    private val client = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    val apiClient: WeatherApiInterface by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        return@lazy retrofit.create(WeatherApiInterface::class.java)
    }
}