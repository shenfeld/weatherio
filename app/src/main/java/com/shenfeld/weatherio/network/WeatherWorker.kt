package com.shenfeld.weatherio.network

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.shenfeld.weatherio.data.DailyDto
import com.shenfeld.weatherio.utils.*
import retrofit2.Call

class WeatherWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {
    override fun doWork(): Result {
        val call = callServer(
            inputData.getDouble("lat", ROSTOV_LAT),
            inputData.getDouble("lon", ROSTOV_LON)
        )
        val response = call.execute()
        if (response.isSuccessful) {
            val responseDailyData = response.body()
            if (responseDailyData != null) {
                dataWeatherFromWorker.postValue(responseDailyData)
            } else
                Result.retry()
        } else
            return Result.failure()
        return Result.success()
    }

    companion object {
        private var dataWeatherFromWorker = MutableLiveData<DailyDto>()
        fun getDataWorker(): LiveData<DailyDto> = dataWeatherFromWorker
    }

    private fun callServer(lat: Double, lon: Double): Call<DailyDto> {
        return WeatherApiClient.apiClient.requestDailyWeather(
            lat,
            lon,
            API_KEY,
            EXCLUDE,
            UNITS
        )
    }
}