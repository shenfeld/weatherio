package com.shenfeld.weatherio.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import com.shenfeld.weatherio.R
import com.shenfeld.weatherio.adapter.WeatherAdapter
import com.shenfeld.weatherio.network.WeatherWorker
import com.shenfeld.weatherio.utils.Cities
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupWorker()
        days_recycler_view.layoutManager = LinearLayoutManager(this)
        WeatherWorker.getDataWorker().observe(this, {
            if (it != null) {
                days_recycler_view.adapter = WeatherAdapter(it.days, R.layout.list_item_day)
                tv_current_main_weather.text =
                    "${it.days[0].weather[0].main}, ${it.days[0].temp.day.roundToInt()}°"
                tv_current_date.text = "Humidity: ${it.days[0].humidity}%"

                val sunset = it.days[0].sunset.toLong() * 1000
                val sunrise = it.days[0].sunrise.toLong() * 1000
                val curTime = Date().time

                if (curTime in sunrise until sunset)
                    iv_current_times_of_day.setImageResource(R.drawable.day)
                else
                    iv_current_times_of_day.setImageResource(R.drawable.night)
            } else {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun setupWorker(city: Cities = Cities.ROSTOV_ON_DON) {
        val coordCity: Data =
            Data.Builder()
                .putDouble("lat", city.lat)
                .putDouble("lon", city.lon).build()
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val uploadWorkRequest: WorkRequest =
            PeriodicWorkRequestBuilder<WeatherWorker>(600, TimeUnit.SECONDS)
                .addTag("WORK")
                .setConstraints(constraints)
                .setInputData(coordCity)
                .build()
        WorkManager.getInstance(applicationContext).enqueue(uploadWorkRequest)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        WorkManager.getInstance(applicationContext).cancelAllWorkByTag("WORK")
        when (item.itemId) {
            R.id.moscow -> {
                setupWorker(Cities.MOSCOW)
                tv_name_city.text = getString(R.string.moscow)
            }
            R.id.rostov -> {
                setupWorker(Cities.ROSTOV_ON_DON)
                tv_name_city.text = getString(R.string.rostov_on_don)
            }
            R.id.krasnodar -> {
                setupWorker(Cities.KRASNODAR)
                tv_name_city.text = getString(R.string.krasnodar)
            }
            R.id.taganrog -> {
                setupWorker(Cities.TAGANROG)
                tv_name_city.text = getString(R.string.taganrog)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        WorkManager.getInstance(applicationContext).cancelAllWorkByTag("WORK")
        super.onDestroy()
    }
}