package com.shenfeld.weatherio.data

import com.google.gson.annotations.SerializedName

class DailyDto (
    @SerializedName("daily") val days: List<DayDto>
)